package org.vitu.service;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.vitu.domain.Pessoa;

import javax.inject.Inject;
import java.util.List;

@Slf4j
@QuarkusTest
class PessoaServiceTest {

    @Inject
    PessoaService pessoaService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findAll() {
        List<Pessoa> pessoas = pessoaService.findAll();
        log.info(pessoas.toString());
        Assertions.assertTrue(pessoas.isEmpty());
    }
}