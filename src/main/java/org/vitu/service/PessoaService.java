package org.vitu.service;

import lombok.extern.slf4j.Slf4j;
import org.vitu.domain.Pessoa;
import org.vitu.repository.PessoaRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@ApplicationScoped
public class PessoaService {

    @Inject
    PessoaRepository pessoaRepository;

    public List<Pessoa> findAll() {
        log.info("Procurando por todas as pessoas");
        return pessoaRepository.listAll();
    }

    @Transactional
    public Pessoa save(Pessoa pessoa) {
        pessoaRepository.persistAndFlush(pessoa);
        return pessoa;
    }

    @Transactional
    public void deleteById(Long id) {
        pessoaRepository.deleteById(id);
    }

    @Transactional
    public Pessoa update(Long id, Pessoa pessoa) {
        Pessoa pessoa1 = pessoaRepository.findByIdOptional(id).orElse(null);
        pessoa1.setCpf(pessoa.getCpf());
        pessoa1.setNome(pessoa.getNome());
        pessoa1.setDataNascimento(pessoa.getDataNascimento());
        pessoaRepository.persistAndFlush(pessoa1);
        return pessoa;
    }

}
