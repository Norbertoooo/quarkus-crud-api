package org.vitu.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.vitu.domain.Pessoa;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class PessoaRepository implements PanacheRepository<Pessoa> {

    public Optional<Pessoa> findByCpf(String cpf) {
        return find("cpf", cpf).singleResultOptional();
    }
}
