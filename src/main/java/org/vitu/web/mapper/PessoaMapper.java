package org.vitu.web.mapper;

import org.vitu.domain.Pessoa;
import org.vitu.web.dto.PessoaDto;

public class PessoaMapper {

    public static PessoaDto toDto(Pessoa pessoa) {
        return PessoaDto.builder()
                .cpf(pessoa.getCpf())
                .dataNascimento(pessoa.getDataNascimento())
                .nome(pessoa.getNome())
                .build();
    }

    public static Pessoa toEntity(PessoaDto pessoaDto) {
        return Pessoa.builder()
                .cpf(pessoaDto.getCpf())
                .dataNascimento(pessoaDto.getDataNascimento())
                .nome(pessoaDto.getNome())
                .build();
    }
}
