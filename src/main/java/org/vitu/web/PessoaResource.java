package org.vitu.web;

import lombok.extern.slf4j.Slf4j;
import org.vitu.domain.Pessoa;
import org.vitu.service.PessoaService;
import org.vitu.web.dto.PessoaDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import static org.vitu.web.mapper.PessoaMapper.toEntity;

@Slf4j
@Path("/pessoa")
@ApplicationScoped
public class PessoaResource {

    @Inject
    PessoaService pessoaService;

    @GET
    public Response findAll() {
        log.info("Requisição para procurar por todas as pessoas");
        return Response.ok(pessoaService.findAll()).build();
    }

    @POST
    public Response save(PessoaDto pessoaDto, @Context UriInfo uriInfo) {
        log.info("Requisição para salvar nova pessoa");
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        Pessoa save = pessoaService.save(toEntity(pessoaDto));
        return Response.created(uriBuilder.build()).entity(save).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id) {
        pessoaService.deleteById(id);
        return Response.noContent().build();
    }

}
